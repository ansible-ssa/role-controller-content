---
- name: GitLab project playbook-windows
  ansible.builtin.import_tasks: add_project.yml
  vars:
    - project_name: GitLab playbook-windows
    - project_url: https://gitlab.com/ansible-ssa/playbook-windows.git

- name: Add Windows Machine Credentials
  ansible.controller.credential:
    name: Windows
    credential_type: Machine
    organization: Default
    state: present
    inputs:
      username: ansible
      password: "{{ controller_admin_password }}"

- name: Setup Windows SMB Server - Job Template
  ansible.controller.job_template:
    name: Windows - SMB Server
    state: present
    inventory: "{{ type }}"
    job_type: run
    playbook: windows-smb-server.yml
    project: GitLab playbook-windows
    allow_simultaneous: true
    limit: "win-server"
    ask_limit: true
    survey_enabled: true
    survey_spec: "{{ lookup('template', 'windows-smb-share-survey.json.j2') }}"
    credentials:
      - Windows
    execution_environment: '{{ "ee-ansible-ssa" if controller_ah_enable | bool else "Default execution environment" }}'

- name: Setup Windows SMB Client - Job Template
  ansible.controller.job_template:
    name: Windows - SMB Client
    state: present
    inventory: "{{ type }}"
    job_type: run
    playbook: windows-smb-client.yml
    project: GitLab playbook-windows
    allow_simultaneous: true
    limit: "win-client"
    ask_limit: true
    survey_enabled: true
    survey_spec: "{{ lookup('template', 'windows-smb-share-survey.json.j2') }}"
    credentials:
      - Windows
    extra_vars:
      windows_server_fqdn: win-server.{{ dns_suffix }}.ansible-labs.de
    execution_environment: '{{ "ee-ansible-ssa" if controller_ah_enable | bool else "Default execution environment" }}'

- name: Create Workflow for Windows SMB Demo
  ansible.controller.workflow_job_template:
    name: Windows - SMB Demo
    description: Deploy Windows SMB Server and SMB Client
    organization: Default
    survey_enabled: true
    survey_spec: "{{ lookup('template', 'windows-smb-share-survey.json.j2') }}"

- name: Add Windows - SMB Client to Windows SMB Demo Workflow
  ansible.controller.workflow_job_template_node:
    identifier: windows-smb-client
    workflow: Windows - SMB Demo
    unified_job_template: Windows - SMB Client
    organization: Default

- name: Add Windows - SMB Server to Windows SMB Demo Workflow
  ansible.controller.workflow_job_template_node:
    identifier: windows-smb-server
    workflow: Windows - SMB Demo
    unified_job_template: Windows - SMB Server
    organization: Default
    success_nodes:
      - windows-smb-client
